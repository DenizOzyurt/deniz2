<div class="card mb-30">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h3>Yeni Ürün Ekleme Formu</h3>

                    <div class="dropdown">
                        <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="bx bx-dots-horizontal-rounded"></i>
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <i class="bx bx-show"></i> Görüntüle
                            </a>
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <i class="bx bx-edit-alt"></i> Düzenle
                            </a>
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <i class="bx bx-trash"></i> Sil
                            </a>
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <i class="bx bx-printer"></i> Yazdır
                            </a>
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <i class="bx bx-download"></i> İndir
                            </a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <form>
                        <div class="form-group">
                            <label>Alan 1</label>
                            <input type="email" class="form-control">
                            <small class="form-text text-muted">Alan hakkındaki uyarı</small>
                        </div>
                        <div class="form-group">
                            <label>Alan 2</label>
                            <input type="email" class="form-control">
							<small class="form-text text-muted">Alan hakkındaki uyarı</small>
                        </div>
                        <button type="submit" class="btn btn-primary">Gönder</button>
                    </form>

                </div>
            </div>